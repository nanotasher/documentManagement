package com.vertafore.ms.documentManagement.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class Document {

    @JsonProperty("id")
    private UUID id = null;
    @JsonProperty("name")
    private String name = "";
    @JsonProperty("filename")
    private String filename = "";
    @JsonProperty("description")
    private String description = "";
    @JsonProperty("createdDate")
    private Date createdDate = new Date();
    @JsonProperty("currentVersion")
    private Version currentVersion = null;
    @JsonProperty("versions")
    private ArrayList<Version> versions = new ArrayList<>();
    @JsonIgnore
    private byte[] bytes;

    public UUID getId() {
        return this.id;
    }
    public void setId(UUID value) { this.id = value; }

    public String getName() {
        return this.name;
    }
    public void setName(String value) { this.name = value; }

    public String getFilename() {
        return this.filename;
    }
    public void setFilename(String value) {
        this.filename = value;
    }

    public String getDescription() {
        return this.description;
    }
    public void setDescription(String value) {
        this.description = value;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }
    public void setCreatedDate(Date value) {
        this.createdDate = value;
    }

    public Version getCurrentVersion() { return this.currentVersion; }
    public void setCurrentVersion(Version value) { this.currentVersion = value; }

    public byte[] getBytes() { return this.bytes; }
    public void setBytes(byte[] value) { this.bytes = value; }
}
