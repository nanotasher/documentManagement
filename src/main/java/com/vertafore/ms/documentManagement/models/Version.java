package com.vertafore.ms.documentManagement.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class Version {
    @JsonProperty("major")
    private int major = 0;
    @JsonProperty("minor")
    private int minor = 0;
    @JsonProperty("patch")
    private int patch = 0;
    @JsonProperty("createdDate")
    private Date createdDate = new Date();

    public String toString() {
        return String.format("%d.%d.%d", major, minor, patch);
    }
}
