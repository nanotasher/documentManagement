package com.vertafore.ms.documentManagement.controllers;

import java.util.ArrayList;
import java.util.UUID;

import com.vertafore.ms.documentManagement.services.DocumentStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import com.vertafore.ms.documentManagement.models.Document;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/documents")
@CrossOrigin(origins = "http://localhost:3000")
public class DocumentsController {

    @Autowired
    private DocumentStore documentStore;

    //  region Public Methods
    //  region GET
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Document> getDocuments() {
        return documentStore.getDocuments();
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public Document getDocument(@PathVariable String id) {
        UUID uuid = UUID.fromString(id);
        return documentStore.getDocument(uuid);
    }

    @GetMapping(value = "/{id}/bytes", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public byte[] getDocumentBytes(@PathVariable String id,
                                   HttpServletResponse response) {
        UUID uuid = UUID.fromString(id);
        Document document = documentStore.getDocument(uuid);

        if (document != null) {
            response.setContentType(getContentType(getFileExtension(document.getFilename())));
            response.setHeader("Content-Disposition", "attachment; filename=\"" + document.getFilename() + "\"");
            return document.getBytes();
        }

        return null;
    }

    //  endregion
    //  endregion

    //  region Private Methods
    private String getContentType(String extension) {
        switch (extension.toLowerCase()) {
            case ".jpg":
                return MediaType.IMAGE_JPEG_VALUE;
            case ".pdf":
                return MediaType.APPLICATION_PDF_VALUE;
            default:
                return MediaType.APPLICATION_OCTET_STREAM_VALUE;
        }
    }

    private String getFileExtension(String filename) {
        if (filename.indexOf('.') == -1)
            return "";

        return filename.substring(filename.lastIndexOf(".") + 1);
    }
    //  endregion
}
