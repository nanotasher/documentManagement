package com.vertafore.ms.documentManagement.services;

import com.vertafore.ms.documentManagement.models.Document;

import java.util.ArrayList;
import java.util.UUID;

public class InMemoryDocumentStore implements DocumentStore {

    private final ArrayList<Document> documents = new ArrayList<>();

    public InMemoryDocumentStore() {

        Document document1 = new Document();
        document1.setName("My Trip to Hawaii");
        document1.setFilename("hawaii.jpg");
        document1.setId(UUID.randomUUID());
        documents.add(document1);

        Document document2 = new Document();
        document1.setName("Bills");
        document1.setFilename("bills.xlsx");
        document2.setId(UUID.randomUUID());
        documents.add(document2);

        Document document3 = new Document();
        document1.setName("Funny picture my friend Bill sent me");
        document1.setFilename("y_u_no_call_me.jpg");
        document3.setId(UUID.randomUUID());
        documents.add(document3);

        Document document4 = new Document();
        document1.setName("Memes");
        document1.setFilename("meme.jpg");
        document4.setId(UUID.randomUUID());
        documents.add(document4);
    }

    public ArrayList<Document> getDocuments() {
        return documents;
    }

    public Document getDocument(UUID uuid) {

        for (Document document : documents) {
            System.out.println("this document id: " + document.getId().toString());
            if (document.getId().toString().equals(uuid.toString())) {
                System.out.println("found it!");
                return document;
            }
        }

        System.out.println("didn't find it");
        return null;
    }

    public Document saveDocument(Document document) {
        for (int i = 0; i < documents.size(); i++) {
            if (documents.get(i).getId().toString().equals(document.getId().toString())) {
                documents.set(i, document);
            }
        }

        return document;
    }

    public void deleteDocument(Document document) {
        for (Document doc : documents) {
            if (doc.getId().toString().equals(document.getId().toString())) {
                documents.remove(doc);
            }
        }
    }
}
