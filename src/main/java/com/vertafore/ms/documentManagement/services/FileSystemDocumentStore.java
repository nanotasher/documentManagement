package com.vertafore.ms.documentManagement.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.vertafore.ms.documentManagement.models.Document;

public class FileSystemDocumentStore implements DocumentStore {

    //  TODO:  Move these to config
    private static final String DOCUMENTS_PATH = "C:\\Projects\\Java\\store\\";
    private static final String LOGGING_PATH = "C:\\Projects\\Java\\log\\";
    private static final String META_FILE = "meta.json";
    private static final ObjectMapper mapper = new ObjectMapper();

    private boolean loggingEnabled = false;

    //  region .ctor
    public FileSystemDocumentStore() {
        //  Ensure documentsPath exists
        File documentsDirectory = new File(DOCUMENTS_PATH);
        if (!documentsDirectory.exists()) {
            createDirectory(documentsDirectory);
        }

        //  Enable pretty print for JSON outputs
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
    }
    //  endregion

    //  region Public Methods
    public ArrayList<Document> getDocuments() {
        ArrayList<Document> documents = new ArrayList<>();

        try {
            File[] directories = new File(DOCUMENTS_PATH).listFiles(File::isDirectory);
            for (File directory : directories) {

                UUID uuid = UUID.fromString(directory.getName());

                Document document = getDocument(uuid);
                if (document != null) {
                    documents.add(document);
                }
            }
        }
        catch (Exception e) {

        }

        return documents;
    }

    public Document getDocument(UUID uuid) {
        Document document = null;

        File directory = new File(DOCUMENTS_PATH, uuid.toString());
        File metafile = new File(directory.getPath(), META_FILE);

        if (metafile.exists()) {
            //  deserialize and add to documents
            document = deserializeMetafile(metafile);
            File documentBytes = new File(directory.getPath(), document.getCurrentVersion().toString());

            Lazy<byte[]> bytes = new Lazy(() -> readBytes(documentBytes.getPath()));
            document.setBytes(bytes.get());
        }

        return document;
    }

    public Document saveDocument(Document document) {

        return new Document();
    }

    public void deleteDocument(Document document) {

    }

    //  endregion

    //  region Private Methods
    private boolean createDirectory(File directory) {
        boolean result = false;

        try {
            result = directory.mkdir();
            log("directory created: " + directory.getName());
        }
        catch (SecurityException se) {
            log("error creating directory: " + se.toString());
        }
        catch (Exception e) {
            log("error creating directory: " + e.toString());
        }

        return result;
    }

    private boolean createFile(File file) {
        boolean result = false;

        try {
            result = file.createNewFile();
            log("file created: " + file.getName());
        }
        catch (SecurityException se) {
            log("error creating file: " + se.toString());
        }
        catch (Exception e) {
            log("error creating file: " + e.toString());
        }

        return result;
    }

    private Document deserializeMetafile(File metafile) {
        Document document = null;

        try {
            String json = readFile(metafile.getPath());
            document = mapper.readValue(json, Document.class);
        }
        catch(IOException e) {
            log("error deserializing meta file: " + e.toString());
        }

        return document;
    }

    private byte[] readBytes(String path) {
        byte[] bytes = null;

        try {
            bytes = Files.readAllBytes(Paths.get(path));
        }
        catch (IOException e) {
            log("could not read bytes: " + e.toString());
        }

        return bytes;
    }

    private String readFile(String path) {
        return new String(readBytes(path));
    }

    private void log(String message) {

        if (!loggingEnabled)
            System.out.println(message);

        String loggingFilename = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime()) + ".log";

        File loggingDirectory = new File(LOGGING_PATH);
        if (!loggingDirectory.exists()) {
            createDirectory(loggingDirectory);
        }

        File loggingFile = new File(LOGGING_PATH, loggingFilename);
        if (!loggingFile.exists()) {
            createFile(loggingFile);
        }

        if (loggingFile.canWrite()) {
            try {
                Files.write(Paths.get(loggingFile.getPath()), (message + "\r\n").getBytes(), StandardOpenOption.APPEND);
                loggingEnabled = true;
            }
            catch (IOException e) {
                System.out.println("cannot append to log file: " + e.toString());
                loggingEnabled = false;
            }
        }
    }
    //  endregion
}
