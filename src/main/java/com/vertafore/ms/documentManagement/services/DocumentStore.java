package com.vertafore.ms.documentManagement.services;

import java.util.ArrayList;
import java.util.UUID;

import com.vertafore.ms.documentManagement.models.Document;

public interface DocumentStore {

    ArrayList<Document> getDocuments();
    Document getDocument(UUID uuid);
    Document saveDocument(Document document);
    void deleteDocument(Document document);
}
