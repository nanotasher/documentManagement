package com.vertafore.ms.documentManagement;

import com.vertafore.ms.documentManagement.services.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DocumentManagementConfig {

    @Bean
    @ConditionalOnProperty(name = "fileSystemDocumentStore")
    public DocumentStore fileSystemDocumentStore() {
        return new FileSystemDocumentStore();
    }

    @Bean
    @ConditionalOnProperty(name = "inMemoryDocumentStore")
    public DocumentStore inMemoryDocumentStore() {
        return new InMemoryDocumentStore();
    }
}
